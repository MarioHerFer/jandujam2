﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapManager : MonoBehaviour {
	public GameObject[, ,]spots; 
	public AudioClip[] SFX;
	public AudioSource audioSource;
	// Use this for initialization
	void Start () {
		spots = new GameObject[6,4,4];
		audioSource= GetComponent<AudioSource>();
	}
	public void AddTakenPosition(int _floor, int _row, int _column, GameObject _voxel)
	{
		if(_floor > 5){
			SceneManager.LoadScene("Title_Screen");
		}
		spots[_floor, _row, _column] = _voxel;
		bool nullInFloor = false;
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				if(spots[_floor, i, j] == null){
					nullInFloor = true;
					return;
				}
			}
		}
		if(!nullInFloor){
			for(int i = 0; i < 4; i++){
				for(int j = 0; j < 4; j++){
					if(spots[_floor,i,j] != null){
						GameObject tempParent = spots[_floor,i,j].transform.parent.gameObject;
						if(tempParent.name.Contains("RedVoxel")){
							StartCoroutine(RedVoxelExplosion(tempParent.transform.position));
						}
						if(tempParent.transform.childCount == 0){
							StartCoroutine(DestroyFloor(tempParent, _floor));
						}
						else{
							StartCoroutine(DestroyFloor(spots[_floor,i,j], _floor));
						}
						spots[_floor,i,j] = null;
					}
				}
			}
		}
	}
	IEnumerator DestroyFloor(GameObject _object, int _floor){
		if(_object)
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				if(_floor < 5){
					if(spots[_floor + 1,i,j] != null){
						spots[_floor + 1,i,j].transform.parent.GetComponent<PieceGravity>().bottomReached = false;
						StartCoroutine(spots[_floor + 1,i,j].transform.parent.GetComponent<PieceGravity>().FallTimer());
					}
				}
				else{
					SceneManager.LoadScene("Title_Screen");
				}
			}
		}
		//ResetArray();
		yield return new WaitForSeconds(0.5f);
		audioSource.PlayOneShot(SFX[0]);
		Destroy(_object);
	}
	IEnumerator RedVoxelExplosion(Vector3 _position){
		audioSource.PlayOneShot(SFX[1]);
		yield return new WaitForSeconds(0.5f);
		for(int i = 0; i < 6; i++){
			for(int j = 0; j < 4; j++){
				for(int y = 0; y < 4; y++){
					if(i == _position.y){
						Destroy(spots[i,j,y]);
						spots[i,j,y] = null;
					}
					if(y == _position.z && j == _position.x){
						Destroy(spots[i,j,y]);
						spots[i,j,y] = null;
					}
				}
			}
		}
	}
	void ResetArray(){
		spots = new GameObject[6,4,4];
	}
}
