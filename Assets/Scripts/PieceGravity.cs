﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceGravity : MonoBehaviour {
	[SerializeField] float fallTimer;
    [SerializeField] CameraRotation CamRot;
	public bool bottomReached;
	public bool falling;
    Vector3 CurrentPos;
    public bool isMoveable;


	void Start()
	{
		bottomReached = false;
		falling = false;
		StartCoroutine(FallTimer());
        CurrentPos = this.transform.position;
        CamRot = GameObject.Find("CameraRotation").GetComponent<CameraRotation>();
    }
	void Update()
	{
        
        if (!bottomReached && !falling){
			StartCoroutine(FallTimer());
		}
        for (int i = 0; i < transform.childCount; i++)
        {
            //Debug.Log(transform.GetChild(i).position);
        }
    }

    private void FixedUpdate()
    {
        if (isMoveable){

            switch (CamRot.position)
            {
                case 1:
                    move13(1);
                    break;

                case 2:
                    move13(1);
                    //move24(1);
                    break;

                case 3:
                    move13(1);
                    //move13(-1);
                    break;

                case 4:
                    move13(1);
                    //move24(-1);
                    break;
            }   
            
        }
        transform.position = CurrentPos;
    }


    private void move13(int j)
    {
        bool isInBoundaries = true;
        Vector3 tempPos = new Vector3();
        tempPos = CurrentPos;
        //Right Left
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            tempPos.x += (1 * j);
            for(int i = 0; i < transform.childCount; i++){
                if(transform.GetChild(i).position.x + (1 * j) >= 4){
                    isInBoundaries = false;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            tempPos.x -= (1 * j);
            for(int i = 0; i < transform.childCount; i++){
                if(transform.GetChild(i).position.x - (1 * j) < 0){
                    isInBoundaries = false;
                }
            }
        }
        //Up Down
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            tempPos.z += (1 * j);
            for(int i = 0; i < transform.childCount; i++){
                if(transform.GetChild(i).position.z + (1 * j) > 4){
                    isInBoundaries = false;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            tempPos.z -= (1 * j);
            for(int i = 0; i < transform.childCount; i++){
                Debug.Log(transform.GetChild(i).position.z);
                if(transform.GetChild(i).position.z - (1 * j) < 1){
                    isInBoundaries = false;
                }
            }
        }

        if(isInBoundaries){
            CurrentPos = tempPos;
            this.transform.position = CurrentPos;
        }
        else{
            CurrentPos = this.transform.position;
        }
    }

    private void move24(int j)
    {
        bool isInBoundaries = true;
        Vector3 tempPos = new Vector3();
        tempPos = CurrentPos;
        //Right Left
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            tempPos.z += (1 * -j);
            for(int i = 0; i < transform.childCount; i++){
                if(transform.GetChild(i).position.z + (1 * -j) > 4){
                    isInBoundaries = false;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            tempPos.z -= (1 * -j);
            for(int i = 0; i < transform.childCount; i++){
                if(transform.GetChild(i).position.z - (1 * -j) < 0){
                    isInBoundaries = false;
                }
            }
        }
        //Up Down
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            tempPos.x += (1 * j);
            for(int i = 0; i < transform.childCount; i++){
                if(transform.GetChild(i).position.x + (1 * j) > 4){
                    isInBoundaries = false;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            tempPos.x -= (1 * j);
            for(int i = 0; i < transform.childCount; i++){
                if(transform.GetChild(i).position.x - (1 * j) <= 0){
                    isInBoundaries = false;
                }
            }
        }
        if(isInBoundaries){
            CurrentPos = tempPos;
            this.transform.position = CurrentPos;
        }
        else{
            CurrentPos = this.transform.position;
        }
    }
    public IEnumerator FallTimer(){
		falling = true;
		yield return new WaitForSeconds(fallTimer);
		if(!bottomReached){
            CurrentPos = new Vector3(transform.position.x, transform.position.y - 1, transform.position.z);
			StartCoroutine(FallTimer());
		}
	}

}
