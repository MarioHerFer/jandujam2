﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceCreate : MonoBehaviour {
    Vector3[] PosVoxelCreator;
    [SerializeField] GameObject[] Pieces;
    int L = 0;
    int i = 0;
    int j = 0;
    // Use this for initialization

    void Start () {
        PosVoxelCreator = new Vector3[4];
        Posinstance();
        SpawnPiece();
    }
    
    private void Posinstance()
    {
        PosVoxelCreator[L] = new Vector3(1.5f + j, 6f, 1.5f + i);
        L++;
        i++;
        PosVoxelCreator[L] = new Vector3(1.5f + j, 6f, 1.5f + i);
        L++;
        j++;
        PosVoxelCreator[L] = new Vector3(1.5f + j, 6f, 1.5f + i);
        L++;
        i--;
        PosVoxelCreator[L] = new Vector3(1.5f + j, 6f, 1.5f + i);
        
        
    }
    

    // Update is called once per frame
    void Update () {
       
    }
    
    public void SpawnPiece()
    {
        GameObject.Find("MapManager").GetComponent<MapManager>().audioSource.PlayOneShot(GameObject.Find("MapManager").GetComponent<MapManager>().SFX[2]);
        int nPiece = Random.Range(0, Pieces.Length);
        int Ran = Random.Range(0, 3);
        GameObject tempPiece = Instantiate(Pieces[nPiece],PosVoxelCreator[Ran],Quaternion.identity);
        tempPiece.GetComponent<PieceGravity>().isMoveable = true;
    }
}
