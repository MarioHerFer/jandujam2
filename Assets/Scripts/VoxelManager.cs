﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelManager : MonoBehaviour {
	PieceGravity pg;
    [SerializeField] PieceCreate pc;
	[SerializeField] MapManager mc;
	public bool isMovable;
	// Use this for initialization
	void Start () {
		pg = GetComponentInParent<PieceGravity>();
		pc = GameObject.Find("Spawned").GetComponent<PieceCreate>();
		mc = GameObject.Find("MapManager").GetComponent<MapManager>();
		isMovable = false;
	}
   
	void OnTriggerEnter(Collider other){
		if(((other.gameObject.tag.Contains("Voxel") && other.transform.position.y < transform.position.y && other.GetComponentInParent<PieceGravity>().bottomReached) || other.gameObject.tag.Contains("MapBase")) && !pg.bottomReached){
			pg.bottomReached = true;
            pg.falling = false;
			
			for(int i = 0; i < pg.transform.childCount; i++){
				mc.AddTakenPosition(Mathf.FloorToInt(pg.transform.GetChild(i).position.y), Mathf.FloorToInt(pg.transform.GetChild(i).position.x), Mathf.FloorToInt(pg.transform.GetChild(i).position.z) - 1, pg.transform.GetChild(i).gameObject);
			}
			//mc.AddTakenPosition(Mathf.FloorToInt(transform.position.y), Mathf.FloorToInt(transform.position.x), Mathf.FloorToInt(transform.position.z) - 1, gameObject);
			if(pg.isMoveable){
				pg.isMoveable = false;
            	pc.SpawnPiece();
			}
        }
	}
	void OnTriggerExit(Collider other){
		if(other.gameObject.tag.Contains("Voxel") && other.transform.position.y < transform.position.y){
			StartCoroutine(pg.FallTimer());
        }
	}
}
