﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {
    float rotationTime;
    Quaternion targetRotation;

    bool rotateLeft;
    bool rotateRight;
    float speed = 0.1f;

    public int position = 1;

    // Use this for initialization
    void Start () {
        rotateLeft = false;
        rotateRight = false;

        //print(position);
    }
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.D))
        {
            switch (position)
            {
                case 1:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y - 90, transform.rotation.z);
                    break;
                case 2:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y + 360, transform.rotation.z);
                    break;
                case 3:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y + 90, transform.rotation.z);
                    break;
                case 4:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y - 180, transform.rotation.z);
                    break;
                default:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y - 90, transform.rotation.z);
                    break;
            }
            //print(targetRotation);
            rotateLeft = true;
            rotationTime = 0;

            position--;

            if(position <= 0)
            {
                position = 4;
            }
            //print(position);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            switch (position)
            {
                case 1:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y + 90, transform.rotation.z);
                    break;
                case 2:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y + 180, transform.rotation.z);
                    break;
                case 3:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y - 90, transform.rotation.z);
                    break;
                case 4:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y + 360, transform.rotation.z);
                    break;
                default:
                    targetRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y + 90, transform.rotation.z);
                    break;
            }

            rotateRight = true;
            rotationTime = 0;

            position++;

            if (position >= 5)
            {
                position = 1;
            }

            //print(position);
        }
        
        if (rotateLeft)
        {
            rotationTime += Time.deltaTime * speed;
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationTime);
        }
        if (rotateRight)
        {
            rotationTime += Time.deltaTime * speed;
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationTime);
        }
        if (transform.rotation == targetRotation)
        {
            rotateLeft = false;
            rotateRight = false;
            rotationTime = 0;
        }
    }
}
